# LaTexSangbogen

[![pipeline status](https://gitlab.com/zunin/LaTexSangbogen/badges/master/pipeline.svg)](https://gitlab.com/zunin/LaTexSangbogen/commits/master)


### Download releases

[Kanotur 2016 edtion](https://github.com/SirCAS/LaTexSangbogen/releases/tag/Kanotur_2016)

[Kanotur 2018 edtion](https://github.com/SirCAS/LaTexSangbogen/releases/tag/Kanotur_2018)
